#include <vector>
#include <numeric>
#include <iostream>
#include <span>
#include <chrono>
using std::chrono::high_resolution_clock;
using std::chrono::duration;

void container_print_uint(const std::span<const unsigned> &container)
{
    for (const unsigned &n : container) {
        std::cout << n << " ";
    }
    std::cout << std::endl;
}

int main()
{
    constexpr unsigned DEGREE = 9000;

//     std::array<unsigned,DEGREE> coefficients;
//     coefficients.fill(1);
//     std::array<unsigned,DEGREE-1> firstDerivCoeff;
//     std::array<unsigned,DEGREE-2> secondDerivCoeff;
//     std::array<unsigned,DEGREE+100> weighted_values;
    std::vector<unsigned> coefficients(DEGREE,1);
    std::vector<unsigned> firstDerivCoeff(DEGREE-1,0);
    std::vector<unsigned> secondDerivCoeff(DEGREE-2,0);
    std::vector<unsigned> weighted_values(DEGREE+100,0);
    std::iota(weighted_values.begin(), weighted_values.end(), 1);

    auto t1 = high_resolution_clock::now();
    std::transform(coefficients.cbegin()+1, coefficients.cbegin()+DEGREE, weighted_values.cbegin(),
        firstDerivCoeff.begin(), std::multiplies<unsigned>());
    std::transform(firstDerivCoeff.cbegin()+1, firstDerivCoeff.cbegin()+(DEGREE-1), weighted_values.cbegin(),
        secondDerivCoeff.begin(), std::multiplies<unsigned>());
    auto t2 = high_resolution_clock::now();
    duration<double, std::nano> time = t2 - t1;
    
    std::cout << "coefficients: " << std::endl;
    container_print_uint(coefficients);
    std::cout << "firstDerivCoeff: " << std::endl;
    container_print_uint(firstDerivCoeff);
    std::cout << "secondDerivCoeff: " << std::endl;
    container_print_uint(secondDerivCoeff);
    
    std::cout << "time: " << time.count() << std::endl;
}
