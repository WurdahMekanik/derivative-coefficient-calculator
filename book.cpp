#include <iostream>
#include <span>
#include <chrono>
using std::chrono::high_resolution_clock;
using std::chrono::duration;

void container_print_uint(const std::span<const unsigned> &container)
{
    for (const unsigned &n : container) {
        std::cout << n << " ";
    }
    std::cout << std::endl;
}

int main()
{
    constexpr unsigned DEGREE = 9000;

    unsigned coefficients[DEGREE];
    unsigned firstDerivCoeff[DEGREE - 1];
    unsigned secondDerivCoeff[DEGREE - 2];
    for (unsigned &i : coefficients) {
        i = 1;
    }

    auto t1 = high_resolution_clock::now();
    for (unsigned i = DEGREE; i > 0; i--) {
        firstDerivCoeff[i-1] = coefficients[i] * i;
    }
    for (unsigned i = DEGREE-1; i > 0; i--) {
        secondDerivCoeff[i-1] = firstDerivCoeff[i] * i;
    }
    auto t2 = high_resolution_clock::now();
    duration<double, std::nano> time = t2 - t1;

    std::cout << "coefficients: " << std::endl;
    container_print_uint(coefficients);
    std::cout << "firstDerivCoeff: " << std::endl;
    container_print_uint(firstDerivCoeff);
    std::cout << "secondDerivCoeff: " << std::endl;
    container_print_uint(secondDerivCoeff);

    std::cout << "time: " << time.count() << std::endl;
}
