Exploring alternate ways of calculating the coefficients for terms in a derivative of a polynomial.

The method in book.cpp was derived from an example found in a book.  
The method in transform.cpp is an attempt to rework the previous method utilizing modern C++ containers & algorithms.  


## Results:
- The "book method" generally takes ~120ms to run on a polynomial of degree 9000  
- The "modern method" generally takes ~25ms to run on a polynomial of degree 9000  
- When using std::array rather than std::vector (i.e. commenting out the std::vector block, and un-commenting the std::array block), the "modern method" performs about as well/poorly as the "book method"